<?php
$startDate = new DateTime("1981-11-03");
$endDate = new DateTime("2013-09-04");

$interval = $startDate->diff($endDate);
echo "Difference : " . $interval->y . " years, " . $interval->m." months, ".$interval->d." days ";
?>